import express from 'express';
import cors from 'cors';
import { configureDb } from './src/configs/DbConfig.js';
import taskRouter from './src/routers/TaskRouter.js';
import userRouter from './src/routers/UserRouter.js';

const app=express();

app.use(cors());
app.use(express.json());
app.use('/tasks',taskRouter);
app.use('/users',userRouter);

app.listen(process.env.PORT,()=>{
    configureDb();
    console.log(`Listening on ${process.env.PORT}`);
})