import express from 'express';
import { getAll, getDashboardData, remove, save, update } from '../controllers/TaskController.js';
import { verifyToken } from '../middlewares/VerifyToken.js';

const taskRouter=express.Router();

taskRouter.post('/',save);
taskRouter.get('/',getAll);
taskRouter.put('/:id',update);
taskRouter.delete('/:id',remove);
taskRouter.get('/dashboard',getDashboardData);

export default taskRouter;
