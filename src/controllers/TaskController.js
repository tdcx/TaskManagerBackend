import {StatusCodes} from 'http-status-codes';
import { Task } from '../models/Task.js';
import { taskSchema } from '../validation-schemas/TaskSchema.js';

export async function save(request,response){
    try {
        const {error,value}=taskSchema.validate(request.body);
        if (error) {
            response.status(StatusCodes.BAD_REQUEST).json({message:'Name is missing'});
        } else {
            const task=new Task(value);
            const savedTask=await task.save();
            response.status(StatusCodes.OK).json(savedTask);
        }
        
    } catch (error) {
        response.status(StatusCodes.INTERNAL_SERVER_ERROR).json({message:'Error in saving task'});
    }
}

export async function getAll(request,response){
    try {
        const tasks=await Task.find();
        response.status(StatusCodes.OK).json(tasks);
    } catch (error) {
        response.status(StatusCodes.INTERNAL_SERVER_ERROR).json({message:'Error in fetching tasks'}); 
    }
}

export async function update(request,response){
    try {
        const task=await Task.findByIdAndUpdate(request.params.id,request.body,{new:true});
        task?response.status(StatusCodes.OK).json(task):response.status(StatusCodes.NOT_FOUND).json({message:'Task not found '})
    } catch (error) {
        response.status(StatusCodes.INTERNAL_SERVER_ERROR).json({message:'Error in updating tasks'}); 
    }
}

export async function remove(request,response){
    try {
        const fetchedTask=await Task.findById(request.params.id);
        if (fetchedTask) {
            if (!fetchedTask.isCompleted) {
                const task=await Task.findByIdAndDelete(request.params.id);
                response.status(StatusCodes.OK).json(task)
            }
            else{
                response.status(StatusCodes.BAD_REQUEST).json({message:'Task is marked complete, it cannot be deleted'});
            }
        }
        else{
            response.status(StatusCodes.NOT_FOUND).json({message:'Task not found '})
        }
    } catch (error) {
        response.status(StatusCodes.INTERNAL_SERVER_ERROR).json({message:'Error in deleting tasks'}); 
    }
}


export async function getDashboardData(request,response){
    try {
        var data={
            tasksCompleted:await Task.countDocuments({isCompleted:true}),
            totalTasks:await Task.countDocuments(),
            latestTasks:await Task.find({},{_id:0,__v:0}).sort({ _id: -1 }).limit(3)
        };
        response.status(StatusCodes.OK).json(data);
    } catch (error) {
        response.status(StatusCodes.INTERNAL_SERVER_ERROR).json({message:'Error in fetching data'});
    }
}
