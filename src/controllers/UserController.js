import { User } from "../models/User.js";
import { userSchema } from "../validation-schemas/UserSchema.js";
import jwt from 'jsonwebtoken';
import { StatusCodes } from "http-status-codes";

export async function login(request,response){
    try {
        const {error,value}=userSchema.validate(request.body);
        if (error) {
            response.status(StatusCodes.BAD_REQUEST).json(error);
        }
        else{
            value['name']=value.name.toLowerCase();
            const user=await User.findOne(value);
            if (!user) {
                response.status(StatusCodes.BAD_REQUEST).json({message:'User not found'});
            } else {
                const token=jwt.sign({userId:user.userId},process.env.SECRET_KEY);
                response.status(StatusCodes.OK).json({token,name:user.name});
            }
        }
        
    } catch (error) {
        console.log(error);
        response.status(StatusCodes.INTERNAL_SERVER_ERROR).json({message:'Error in login'}); 
    }
}