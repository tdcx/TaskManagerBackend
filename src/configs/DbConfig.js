import mongoose from "mongoose";
import 'dotenv/config';

export async function configureDb(){
    try {
        await mongoose.connect(process.env.DB_URI,{ useNewUrlParser: true, useUnifiedTopology: true });
        console.log("Connected!");
    } catch (error) {
        console.log("Could not connect "+error);
    }
}