import mongoose from "mongoose";

const taskSchema=new mongoose.Schema({
    name:{type:String,required:true},
    isCompleted:{type:Boolean,default:false,required:true}
});

export const Task=mongoose.model('task',taskSchema);