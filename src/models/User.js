import mongoose from "mongoose";

const userSchema=new mongoose.Schema({
    userId:{type:Number,required:true,unique:true},
    name:{type:String,required:true}
});

export const User=mongoose.model('user',userSchema);