import Joi from "joi";

export const userSchema = Joi.object({
    userId: Joi.number().required(),
    name: Joi.string().required()
});