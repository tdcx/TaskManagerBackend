import { StatusCodes } from "http-status-codes";
import jwt from 'jsonwebtoken';
import 'dotenv/config';

export function verifyToken(request,response,next){
    try {
        const authHeader=request.get('Authorization');
        if (authHeader) {
            const token=authHeader.split(' ')[1];
            jwt.verify(token,process.env.SECRET_KEY,(error,payload)=>{
                if (error) {
                    response.status(StatusCodes.UNAUTHORIZED).json({message:'Unauthorized access. Invalid token'});
                } else {
                    next();
                }
            });
        }
        else{
            response.status(StatusCodes.UNAUTHORIZED).json({message:'Unauthorized access. Token is missing'});
        }
    } catch (error) {
        response.status(StatusCodes.INTERNAL_SERVER_ERROR).json({message:'Error in verifying token'});
    }
}